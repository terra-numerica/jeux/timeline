import base64
import os
from pathlib import Path

from jinja2 import Environment, FileSystemLoader
from slugify import slugify

from timeline_py.settings import ENIGME_SOL, theme_dict

INDEX_HTML = "www/index.html"
ESCAPE_DIR = Path("www/escape")
ESCAPE_INDEX = ESCAPE_DIR / "index.html"
ESCAPE_ROOM_DIR = ESCAPE_DIR / "room"
ESCAPE_ROOM = ESCAPE_ROOM_DIR / "index.html"


if not os.path.exists(ESCAPE_ROOM_DIR):
    os.makedirs(ESCAPE_ROOM_DIR)
# Définir le dossier de templates
dossier_templates = "www/templates"

# Créer un environnement Jinja avec le chargeur de fichiers
env = Environment(loader=FileSystemLoader(dossier_templates))


HEAD = '<div class="reveal"><div class="slides">'
FOOT = "</div></div>"
# SLIDE = """<div class="slides">
#         <section>Horizontal Slide</section>
#         <section>
#           <section>Vertical Slide 1</section>
#           <section>Vertical Slide 2</section>
#         </section>
#       </div>"""


def _generate_index(input_cards: list):
    """Génère la page d'index avec le diaporama"""
    print(f"Generating {INDEX_HTML}")
    html = HEAD
    # create a verical slideshow for each subject
    # type, year, title, picture, description, credits, escape
    theme = input_cards[0]["type"]
    nom_theme = theme_dict[theme]["nom"]
    r, g, b = theme_dict[theme]["color"].split(",")
    html += f'<section class="{theme}" data-background="rgb({r},{g},{b})">\n<section><h2>{nom_theme}</h2></section>\n'
    for card in input_cards:
        # met à jour le thème en cas de changement et ajoute un section
        if theme != card["type"]:
            theme = card["type"]
            nom_theme = theme_dict[theme]["nom"]
            r, g, b = theme_dict[theme]["color"].split(",")
            html += f"""</section>
            <section class="{theme}" data-background="rgb({r},{g},{b})">
              <section>
              <h2>{nom_theme}</h2>
              </section>"""
        title = card["title"]
        html += f'<section><h3>{card["year_html"]}: {title}</h3>\n'
        html += f"""<div class="flex">
        <div class="left">{card["description_html"]}</div>
        <div class="right rot-caption">
          <figure class="figure">
            <img src="/img/{slugify(title)}.jpg" class="figure-img img-fluid rounded" alt="{title}"/>
          <figcaption class="small">{card["credits"]}</figcaption>
          </figure></div>
        </div>
        </section>"""
    html += "</section>\n"
    html += FOOT

    # Charger le template HTML
    template = env.get_template("index.jinja")

    # Rendu du template
    html_rendu = template.render(slide_content=html)

    # Enregistrer le rendu HTML dans un fichier
    with open(INDEX_HTML, "w") as f:
        f.write(html_rendu)


def _generate_escape():
    """Génère les pages pour l'escape game"""
    print(f"Generating {ESCAPE_INDEX}")
    # Page d'index avec les divers documents
    template = env.get_template("escape.jinja")
    html_rendu = template.render()
    with open(ESCAPE_INDEX, "w") as f:
        f.write(html_rendu)

    # Page de l'escape room
    print(f"Generating {ESCAPE_ROOM}")
    template = env.get_template("room.jinja")
    # Encode en base 64 la solution dans la page html
    sol64 = base64.b64encode(str(ENIGME_SOL).encode("ascii")).decode("ascii")
    html_rendu = template.render(ENIGME_SOL=sol64)
    with open(ESCAPE_ROOM, "w") as f:
        f.write(html_rendu)


def generate_web(input_cards: list):
    """Génère les page web à partir des données"""
    _generate_index(input_cards)
    _generate_escape()
