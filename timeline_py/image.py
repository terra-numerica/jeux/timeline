import io
import os
import shutil
import tempfile

import numpy as np
import requests
from PIL import Image
from slugify import slugify

# Configuration
IMG_PATH = "./www/public/img/"

name = "Timeline cards tex generator"
homepage = "https://framagit.org/terra-numerica/jeux/timeline"

# Convert these constants to uppercase
golden_height = 980
golden_width = 680
golden_ratio = golden_height / golden_width
strip_width_percent = 0.1666
ratio_threshold = 0.02


# Helpers
def download_picture(card_obj):
    filename = pict_filename(card_obj, "_web_original")
    if not os.path.exists(filename):
        uri = card_obj["picture"]
        if uri.startswith("http"):
            download(uri, filename)
            print(f"Téléchargement de {filename}")
        else:
            print(f"Copie de {filename}")
            shutil.copyfile(uri.strip(), filename)


def pict_filename(card_obj, suffix=""):
    # TODO Not optimal because extension is hard-coded
    # but we need jpg for latex
    return f'{IMG_PATH}{slugify(card_obj["title"])}{suffix}.jpg'


# Download an image from an uri and save it to a file as jpeg
def download(uri, filename):
    print("Downloading", uri, "to", filename)
    agent = f"{name} ({homepage}) python-requests/{requests.__version__}"
    headers = {"User-Agent": agent}
    response = requests.get(uri, headers=headers, stream=True)
    # write bytes to disk
    buffer = tempfile.SpooledTemporaryFile(max_size=1e9)
    response.raise_for_status()

    for chunk in response.iter_content(chunk_size=8192):
        buffer.write(chunk)

    buffer.seek(0)
    im = Image.open(io.BytesIO(buffer.read()))
    buffer.close()
    # Save to jpeg
    channels = im.split()
    if len(channels) > 3:
        # convert RGBA to RGB with white background
        background = Image.new("RGB", im.size, color=(255, 255, 255))
        background.paste(im, mask=channels[3])
        background.save(filename, "JPEG", quality=95)
    else:
        im.convert("RGB").save(filename, "JPEG", quality=95)


def calculate_credit_color(card):
    """Calculate best credit_color image for a card"""
    if card.get("credits_color"):
        return

    # Calculate pixel average value of the upper right part of image where the credits is writed
    fname = pict_filename(card)
    im = Image.open(fname).convert("L")
    im = np.array(im)
    # crop upper right part of image
    im_crop = im[95 * im.shape[0] // 100 :, : 3 * im.shape[1] // 4]
    mean = im_crop.mean()

    # ATTENTION modifie la carte par référence!!!
    # TODO maybe could persist this value
    card["credits_color"] = "white" if mean < 128 else "black"


# Resize images according to their format with the ideal one
# and calculate best color for credits
def resizer(card):
    in_fname, out_fname = pict_filename(card, "_web_original"), pict_filename(card)

    if os.path.exists(out_fname):
        return

    print(f"Resizing {in_fname} to {out_fname}")
    image = Image.open(in_fname)
    width, height = image.size
    ratio = height / width
    target = {
        "width": width,
        "height": height,
        "left": 0,
        "top": 0,
        "flag": "normal",
    }

    if ratio > golden_ratio * (1 + ratio_threshold):
        target["height"] = round(width * golden_ratio)
        target["top"] = round((height - width * golden_ratio) / 2)
        target["flag"] = "vertical"
    elif ratio < golden_ratio * (1 - ratio_threshold):
        target["width"] = round(height / golden_ratio)
        target["left"] = (width - height / golden_ratio) / 2
        strip_width = (strip_width_percent * height) / golden_ratio
        delta = min(strip_width, target["left"])
        target["left"] = round(target["left"] - delta)
        target["flag"] = "horizontal"

    image = image.crop(
        (
            target["left"],
            target["top"],
            target["width"] + target["left"],
            target["height"] + target["top"],
        )
    )
    image = image.resize((golden_width, golden_height))
    image.save(out_fname, "JPEG", quality=95)


def download_images(cards):
    for card in cards:
        download_picture(card)
        resizer(card)
        calculate_credit_color(card)
