import seaborn as sns

# Card types for the game
# les noms sont écrits sur le côté des cartes
# les descriptions sont écrites sur les plateaux de jeux numérotés de l'escape
# game afin de pouvoir les classer
theme_dict = {
    "ai": {
        "nom": "Intelligence artificielle",
        "description": "Domaine qui concerne la création de machines intelligentes.",
    },
    "hard": {
        "nom": "Matériel",
        "description": "Comprend la conception de composants physiques tels que les processeurs.",
    },
    "game": {
        "nom": "Graphisme et jeux vidéos",
        "description": "Englobe la conception d'éléments visuels et interactifs pour les jeux.",
    },
    "mem": {
        "nom": "Mémoires",
        "description": "Capacité d'un système informatique à stocker et récupérer des données.",
    },
    "web": {
        "nom": "Réseaux et web",
        "description": "Implique les infrastructures pour la communication à travers le monde.",
    },
    "pop": {
        "nom": "Culture populaire",
        "description": "En rapport avec les éléments ayant un attrait et une influence sur la société.",
    },
    "soft": {
        "nom": "Logiciels",
        "description": "Catégorie de programmes informatiques pour effectuer des tâches spécifiques.",
    },
    "lang": {
        "nom": "Langages de programmation",
        "description": "Règles pour écrire des instructions informatiques.",
    },
    "algo": {
        "nom": "Algorithmes",
        "description": "Séquences d'étapes pour résoudre des problèmes.",
    },
    "crypto": {
        "nom": "Cryptographie",
        "description": "Sécurisation des communications par chiffrement.",
    },
    "robot": {
        "nom": "Robots",
        "description": "Machines capables d'effectuer des tâches programmées de manière autonome ou semi-autonome.",
    },
}

# Enigme et sa résolution
ENIGME = "DATE DU PREMIER ORDINATEUR + APPARITION DU WEB - DATE DE LA CARTE DOUBLE"
assert len(ENIGME) % 9 == 0, "L'énigme n'est pas un multiple de 9"
ENIGME_RESOL = "ENIAC(1945) + WWW(1990) - AlphaZero qui fait partie des thèmes IA et algorithmes(2017)"
ENIGME_SOL = 1918

# color palette from seaborn


palette = [
    f"{int(255*r)},{int(255*g)},{int(255*b)}"
    for r, g, b in sns.color_palette("hls", len(theme_dict.keys()))
]

for i, t in enumerate(theme_dict):
    theme_dict[t]["color"] = palette[i]
