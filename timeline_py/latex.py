import re
from pathlib import Path

from timeline_py.image import pict_filename
from timeline_py.settings import ENIGME, theme_dict

LATEX_PATH = "./latex/"
TIMELINE_TEX = "./cartes_timeline.tex"
ESCAPE_TEX = "./cartes_escape.tex"
PLATEAU_TEX = "./plateau_escape.tex"
CORRECTION_TEX = "./correction_escape.tex"
GUIDE_TEX = "guide.tex"
SPECIAL_CARDS = Path(LATEX_PATH) / "special_cards"
RE_DATE = re.compile(r"\d\d\d\d")


def card_type(card_obj: dict, col_desc: str = "type_nom"):
    """Return the latex code for the card type"""
    r, g, b = [int(v) for v in card_obj["type_color"].split(",")]
    theme = card_obj[col_desc]
    return f"\\cardtype{{{r}}}{{{g}}}{{{b}}}{{{theme}}}"


def front_filename(card_obj, game_type: str):
    if card_obj == "blanck":
        return str(SPECIAL_CARDS / "00_blank.tex")
    elif card_obj == "rules":
        if game_type == "timeline":
            return str(SPECIAL_CARDS / "0_rules_front.tex")
        elif game_type == "escape":
            return str(SPECIAL_CARDS / "0_rules_front_escape.tex")


def back_filename(card_obj, game_type: str):
    if card_obj == "blanck":
        return str(SPECIAL_CARDS / "00_blank.tex")
    elif card_obj == "rules":
        if game_type == "timeline":
            return str(SPECIAL_CARDS / "0_rules_back.tex")
        elif game_type == "escape":
            return str(SPECIAL_CARDS / "0_rules_back_escape.tex")


# Main content of .tex files associated with a card
def front_content(card_obj):
    credits_color = card_obj["credits_color"]
    title = card_obj["title"]
    assert (
        len(title) < 69
    ), f"Title of {card_obj['title']} is too long, {len(title)} >= 69"
    credits = card_obj["credits"]
    assert (
        len(credits) < 100
    ), f"Credits of {card_obj['title']} is too long, {len(credits)} >= 100"
    return f"""
\\begin{{tikzpicture}}
    \\cardborder
    \\cardfrontbackground{{{pict_filename(card_obj)}}}
    {card_type(card_obj)}
    \\cardtitle{{{title}}}
    \\cardcredits[{credits_color}]{{{credits}}}
\\end{{tikzpicture}}
"""


def back_content(card_obj: dict, idx: int, game_type: str):
    description = card_obj["description"]
    N = len(description)
    assert (
        N < 505
    ), f"Description of {card_obj['title']} is too long, {N} >= 505 \n\n{description}"
    # passage en latex
    description = card_obj["description_latex"]
    if game_type == "escape":
        # écrit le code ASCII da la lettre de l'énigme à la place de la date
        year = to_bin_ascii(ENIGME[idx])
        # utilise le titre comme descriptif à gauche
        # pour garder un idée de la carte lors du classement sur le plateau
        col_bandeau = "title"
        # enlève les dates de la description
        description = RE_DATE.sub(r"????", description)

    else:
        year = card_obj["year_latex"]
        col_bandeau = "type_nom"

    # markdown -> latex

    return f"""
\\begin{{tikzpicture}}
    \\cardborder
    \\cardbackbackground{{{pict_filename(card_obj)}}}
    {card_type(card_obj, col_bandeau)}
    \\cardcontent{{{year}}}{{{description}}}
\\end{{tikzpicture}}
"""


# Main PDF with nine cards per page
def generate_latex_nine_cards_by_page(cards, game_type: str):
    print("Generating 3x3 plates")
    # adapt card list to have a multiple of 9 cards with rules and blank cards
    N = len(cards)

    document = ""
    for i in range(0, 9 * (N + 1) // 9, 9):
        document += "  \\begin{center}\n    \\begin{tabular}{@{}c@{\\hspace{2mm}}c@{\\hspace{2mm}}c@{}}%\n"
        for j in range(3):
            for k in range(3):
                idx = i + j * 3 + k
                if idx == N:
                    document += f"      \\input{{{front_filename('rules', game_type)}}}"
                elif idx > N:
                    document += (
                        f"      \\input{{{front_filename('blanck', game_type)}}}"
                    )
                else:
                    document += f"      {front_content(cards[idx])}"
                if k < 2:
                    document += "&%\n"
                else:
                    document += "\\\\%\n"

        document += "    \\end{tabular}\n    \\end{center}\n    \\clearpage%\n"

        document += "  \\begin{center}\n    \\begin{tabular}{@{}c@{\\hspace{2mm}}c@{\\hspace{2mm}}c@{}}%\n"
        for j in range(3):
            for k in range(2, -1, -1):
                idx = i + j * 3 + k
                if idx == N:
                    document += f"      \\input{{{back_filename('rules', game_type)}}}"
                elif idx > N:
                    document += f"      \\input{{{back_filename('blanck', game_type)}}}"
                else:
                    document += f"      {back_content(cards[idx], idx, game_type)}"
                if k > 0:
                    document += "&%\n"
                else:
                    document += "\\\\%\n"

        document += "    \\end{tabular}\n    \\end{center}\n    \\clearpage%\n"

    latex_content = f"""
\\documentclass[a4paper]{{article}}

\\input{{./{LATEX_PATH}/packages}}
\\input{{./{LATEX_PATH}/tikzcards}}

\\geometry{{hmargin=5mm, vmargin=10mm}}

\\begin{{document}}%
{document}
\\end{{document}}
"""
    tex_file = TIMELINE_TEX if game_type == "timeline" else ESCAPE_TEX
    with open(tex_file, "w") as f:
        f.write(latex_content)


def generate_plateau(cards):
    """Génére des plateaux de jeu avec les dates des 9 cartes du thème"""
    N = len(cards)
    # vérifie que c'est un multiple de 9
    assert N == len(
        ENIGME
    ), f"Nombre de cartes ({N}) n'est pas compatible avec l'énigme"
    themes = []
    document = ""
    for i in range(0, 9 * N // 9, 9):
        theme = cards[i]["type"]
        # nouveau thème
        if theme not in themes:
            themes.append(theme)
        else:
            raise Exception(f"Thème déjà utilisé: {theme}")

        # vérifie la cohérence des thèmes
        for j in range(9):
            assert cards[i + j]["type"] == theme, f"Thème incohérent: {theme}"

        print(f"Generation du plateau de jeu: {theme}")
        title = f'\\textbf{{Thème {1+i//9}:}} {theme_dict[theme]["description"]}'
        document += "  \\begin{center}%\n"
        document += f"""  \\begin{{tikzpicture}}
  \\matrix [row sep=1cm,column sep=3cm](m)
  {{\\cardEscapeBorderWithDate{{{cards[i]["year_latex"]}}}; & \\cardEscapeBorderWithDate{{{cards[i+1]["year_latex"]}}}; & \\cardEscapeBorderWithDate{{{cards[i+2]["year_latex"]}}} {{6}}; \\\\
    \\cardEscapeBorderWithDate{{{cards[i+3]["year_latex"]}}}; & \\cardEscapeBorderWithDate{{{cards[i+4]["year_latex"]}}}; & \\cardEscapeBorderWithDate{{{cards[i+5]["year_latex"]}}} {{6}}; \\\\
    \\cardEscapeBorderWithDate{{{cards[i+6]["year_latex"]}}}; & \\cardEscapeBorderWithDate{{{cards[i+7]["year_latex"]}}}; & \\cardEscapeBorderWithDate{{{cards[i+8]["year_latex"]}}} {{6}}; \\\\
  }};
  \\node[font=\\Huge,anchor=south,inner sep=5pt, outer sep=2cm, text width=0.9\linewidth, align=center, fill=lightgray,elementcorners] at (m.north) {{{title}}};
\end{{tikzpicture}}%\n\end{{center}}%\n"""

        document += "    \\clearpage%\n"

    latex_content = f"""
\\documentclass[a3paper]{{article}}

\\input{{./{LATEX_PATH}/packages}}
\\input{{./{LATEX_PATH}/tikzcards}}

\\geometry{{hmargin=5mm, vmargin=10mm}}

\\begin{{document}}%
{document}

\\end{{document}}
"""
    with open(PLATEAU_TEX, "w") as f:
        f.write(latex_content)


def to_bin_ascii(s: str) -> str:
    "Convertit une lettre en code ASCII binaire"
    if s == "SPACE":
        s = " "
    # converti en entier
    ascii_int = ord(s)
    assert ascii_int < 128, f"{s} ne fait pas partie de ASCII"

    return "{0:b}".format(ascii_int).zfill(7)


def _generate_correction(cards):
    """Génère une correction pour l'animateur"""
    N = len(cards)
    themes = []
    document = ""

    def correction(i: int):
        """Crée un texte de correction pour une carte"""
        card = cards[i]
        return f'\\textbf{{{ENIGME[i]}}}| {card["title"]} ({card["year_latex"]})'

    for i in range(0, 9 * N // 9, 9):
        theme = cards[i]["type"]
        # nouveau thème
        if theme not in themes:
            themes.append(theme)
        else:
            raise Exception(f"Thème déjà utilisé: {theme}")

        # vérifie la cohérence des thèmes
        for j in range(9):
            assert cards[i + j]["type"] == theme, f"Thème incohérent: {theme}"

        print(f"Generation du plateau de jeu: {theme}")
        document += f"\\subsection*{{Thème {1+i//9}: {theme_dict[theme]['nom']}}}\n"
        document += "  \\begin{center}%\n"
        document += f"""  \\begin{{tabularx}}{{1.0\\textwidth}}{{ |X|X|X|}}
  \\hline
  {correction(i+0)} & {correction(i+1)} & {correction(i+2)} \\\\
  \\hline
  {correction(i+3)} & {correction(i+4)} & {correction(i+5)} \\\\
  \\hline
  {correction(i+6)} & {correction(i+7)} & {correction(i+8)} \\\\
  \\hline
    \end{{tabularx}}%\n
  \\end{{center}}%\n"""

    latex_content = f"""
\\documentclass[a4paper]{{article}}

\\input{{./{LATEX_PATH}/packages}}
\\usepackage{{tabularx}}

\\geometry{{hmargin=10mm, vmargin=10mm}}

\\begin{{document}}%
\\section*{{Correction}}
{document}

\\end{{document}}
"""
    with open(CORRECTION_TEX, "w") as f:
        f.write(latex_content)


# Main program loop
def generate_tex(input_cards, game_type):
    if game_type == "escape":
        cards = [card for card in input_cards if isinstance(card[game_type], str)]
        print("Génération du plateau de jeu")
        generate_plateau(cards)
        _generate_correction(cards)
    else:
        cards = [card for card in input_cards]

    print(f"Generating timeline with {len(cards)} cards")
    generate_latex_nine_cards_by_page(cards, game_type)


def generate_guide(enigme: str, enigme_resol: str, enigme_sol: int):
    """Génère le guide de jeu avec l'énigme à résoudre

    y place l'énigme et sa résolution et l'écrit à la racine du projet"""
    print("Generating guide.tex")
    with open(Path(LATEX_PATH) / GUIDE_TEX) as f, open(GUIDE_TEX, "w") as g:
        for line in f.readlines():
            g.write(
                line.replace("{{ ENIGME }}", enigme)
                .replace("{{ ENIGME_RESOL }}", enigme_resol)
                .replace("{{ ENIGME_SOL }}", str(enigme_sol))
            )
