"""Lis et augmente les données et TODO les met en cache"""

import re
import warnings
from html.parser import HTMLParser

import pandas as pd
from docutils.core import publish_string
from myst_parser.docutils_ import Parser

from .image import download_images
from .settings import theme_dict

warnings.simplefilter(action="ignore", category=FutureWarning)

RE_LATEX = re.compile(r"\\begin\{document\}\n\n(.*)\n\n\\end\{document\}", re.DOTALL)
RE_HTML = re.compile(r"<main>\n\n\n(.*)\n</main>", re.DOTALL)


def myst_to(md, format=str):
    """Parse a string of MyST markdown into a docutils document.

    see: https://myst-parser.readthedocs.io/en/latest/docutils.html
    """
    return publish_string(source=md, writer_name=format, parser=Parser()).decode(
        "utf-8"
    )


class MyHTMLParser(HTMLParser):
    """Convert html to plain text

    from: https://blog.finxter.com/5-best-ways-to-convert-python-html-string-to-text/
    """

    def __init__(self):
        super().__init__()
        self.text = ""

    def handle_data(self, data):
        self.text += data


def prepare_data(csv_file):
    # read csv as list of dicts
    input_cards = pd.read_csv(csv_file)
    print(f"Read {len(input_cards)} cards from {csv_file}")

    # Augmente les données avec les types de cartes
    input_cards["type_nom"] = input_cards["type"].apply(lambda x: theme_dict[x]["nom"])
    input_cards["type_color"] = input_cards["type"].apply(
        lambda x: theme_dict[x]["color"]
    )

    # conversion en liste de dictionnaires
    input_cards = input_cards.reset_index().to_dict("records")

    # attention input_cards est passée par référence et modifiée
    download_images(
        input_cards
    )  # TODO cache inplace change ajoute une colonne credits_color

    # process markdown avec docutils et myst
    # # TODO cache inplace change les descriptions et titres
    print("Processing markdown")
    for card in input_cards:
        print("conversion md de", card["title"])

        # L'année peut être un siècle avec du markdown
        year = card["year"]
        try:
            year = int(year)
            year_latex = year
            year_html = year
        except ValueError:
            year_latex = myst_to(year, format="latex")
            year_latex = RE_LATEX.search(year_latex).group(1)
            year_html = myst_to(year, format="html5")
            year_html = RE_HTML.search(year_html).group(1)

        card["year_latex"] = year_latex
        card["year_html"] = year_html

        # description
        description = card["description"]
        description_latex = myst_to(description, format="latex")
        description_latex = RE_LATEX.search(description_latex).group(1)
        card["description_latex"] = description_latex
        description_html = myst_to(description, format="html5")
        description_html = RE_HTML.search(description_html).group(1)
        card["description_html"] = description_html
        # enlève les balises html pour compter les caractères
        parser = MyHTMLParser()
        parser.feed(description_html)
        card["description"] = parser.text

    return input_cards
