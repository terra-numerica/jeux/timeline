import argparse
import os

from timeline_py.data import prepare_data
from timeline_py.image import IMG_PATH
from timeline_py.latex import generate_guide, generate_tex
from timeline_py.settings import ENIGME, ENIGME_RESOL, ENIGME_SOL
from timeline_py.website import generate_web

# Initialize argparse.ArgumentParser
parser = argparse.ArgumentParser()

# Command line program
main = "build.py"
description = "Generate tex files for Timeline cards from a csv file"

parser.add_argument(
    "csv_file",
    nargs="?",
    help="The csv input file",
    default="content/timeline.csv",
)
parser.add_argument(
    "--build",
    choices=["tex", "html"],
    nargs="?",
    help="""Type of cards game to generate.
    timeline: timeline cards
    escape: escape game""",
    default="all",
)
args = parser.parse_args()


# Create directories if they don't exist
if not os.path.exists(IMG_PATH):
    os.makedirs(IMG_PATH)


input_cards = prepare_data(args.csv_file)

print("Building target:", args.build)
if args.build in ["all", "html"]:
    generate_web(input_cards)
if args.build in ["all", "tex"]:
    generate_tex(input_cards, game_type="timeline")
    generate_tex(input_cards, game_type="escape")
    generate_guide(ENIGME, ENIGME_RESOL, ENIGME_SOL)
