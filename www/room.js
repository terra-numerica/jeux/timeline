import confetti from "canvas-confetti";
import "./room.scss";
const publicPath = import.meta.env.BASE_URL;
// Date et heure de fin
let deadline;
// joue les sons
let sonAleatoires
const ambianceOrage = new Audio(
  `${publicPath}/son/STORM_Pluie et orage 2 (ID 0740)_LS.mp3`,
);

// Récupère le code de solution dans le html
const ENIGME_SOL = Number(atob(document.querySelector("#solution").dataset.solution));

// Lance le jeu à l'appui du bouton
document.querySelector("#start").addEventListener("click", (el) => {
  const duree = Number(el.target.parentElement.querySelector("#duree").value);
  console.log("Lancement du jeu pour", duree, "minutes")
  document
    .querySelectorAll(".before")
    .forEach((e) => e.classList.add("d-none"));
  document
    .querySelectorAll(".jeu")
    .forEach((e) => e.classList.remove("d-none"));
  toggleFullScreen();
  deadline = new Date().getTime() + duree * 60 * 1000;
  cptARebours();
  playSounds();
});

// Vérifie le code entré
const verifCode = () => {
  const code = Number(document.querySelector("#code").value);
  if (code === ENIGME_SOL) {
    document.querySelector("body").classList.add("bg-secondary");
    document.querySelectorAll(".jeu").forEach((e) => e.classList.add("d-none"));
    document.querySelectorAll("h1").forEach((e) => e.classList.add("no-glitch"));
    document
      .querySelectorAll(".gagne")
      .forEach((e) => e.classList.remove("d-none"));
    clearInterval(sonAleatoires);
    ambianceOrage.pause();
    // lance les feux d'artifice
    firework();

  }
  //  enleve une minute si le code est faux
  document.getElementById("countdown").innerHTML = "MAUVAIS CODE, PERTE D'UNE MINUTE :(";
  deadline = deadline - 60 * 1000;
}
document.querySelector("#verif").addEventListener("click", verifCode);
// from https://developer.mozilla.org/en-US/docs/Web/API/KeyboardEvent/key#examples
document.querySelector("#code").addEventListener(
  "keydown",
  (event) => {
    if (event.defaultPrevented) {
      return; // Do nothing if the event was already processed
    }

    switch (event.key) {
      case "Enter":
        // Do something for "enter" or "return" key press.
        verifCode();
        break;
      default:
        return; // Quit when this doesn't handle the key event.
    }

    // Cancel the default action to avoid it being handled twice
    event.preventDefault();
  },
  true,
);


// Lance les sons aléatoires
const playSounds = () => {
  //  lance la musique d'ambiance orage

  ambianceOrage.play();

  ambianceOrage.loop = true;

  // lance les sons aléatoires
  var vaisseau = new Audio(
    `${publicPath}/son/SCIShip_Passage vaisseau spatial 5 (ID 2036)_LS.mp3`,
  );
  vaisseau.volume = 0.7;

  var pierreTombale = new Audio(
    `${publicPath}/son/ROCKFric_Pierre tombale ouverture (ID 0580)_LS.mp3`,
  );
  pierreTombale.volume = 0.3;
  
  var clavier = new Audio(
    `${publicPath}/son/CMPTKey_Clavier d ordinateur (ID 0229)_LS.mp3`,
  );
  clavier.volume = 0.5;
  
  const porte = new Audio(
    `${publicPath}/son/DOORCreak_Grincement de porte (ID 0302)_LS.mp3`,
  );
  porte.volume = 0.5;
  
  const sons = [porte, vaisseau, pierreTombale, clavier];

  // ajouter rire de sorcière, porte qui grince, chauve-souris, loup, hibou, etc.
  sonAleatoires = setInterval(function () {
    const son = sons[Math.floor(Math.random() * sons.length)];
    setTimeout(
      () => {
        son.play();
      },
      Math.floor(Math.random() * 30) * 1000,
    );
  }, 60 * 1000);
};

/**
 * Passe en plein écran
 *
 */
function toggleFullScreen() {
  if (!document.fullscreenElement) {
    document.documentElement.requestFullscreen();
  } else if (document.exitFullscreen) {
    document.exitFullscreen();
  }
}

// Date et heure actuelle
const cptARebours = () => {
  // Mettre à jour le compte à rebours toutes les secondes
  var periodicTime = setInterval(function () {
    // Date et heure actuelle
    var now = new Date().getTime();

    // Calculer la différence entre la date limite et la date actuelle
    var remaining = deadline - now;

    // Calculer les minutes et secondes restantes
    var minutes = String(
      Math.floor((remaining % (1000 * 60 * 60)) / (1000 * 60)),
    ).padStart(2, "0");
    var seconds = String(Math.floor((remaining % (1000 * 60)) / 1000)).padStart(
      2,
      "0",
    );
    const datetime = `${minutes}:${seconds}`;
    // Afficher le compte à rebours
    const timeEl = document.getElementById("countdown");
    timeEl.innerHTML = datetime;
    timeEl.datetime = datetime;
    // Si le compte à rebours est terminé, afficher un message
    if (remaining < 0) {
      clearInterval(periodicTime);
      document.getElementById("countdown").innerHTML = "TEMPS ÉCOULÉ !";
    }
  }, 1000);
};


const firework = () => {
  // 1minute 30 secondes de son et de feux d'artifice
  var sonArtifice = new Audio(
    `${publicPath}/son/"FRWKComr_Feu d artifice 2 (ID 2672)_LS.mp3"`,
  );
  sonArtifice.play();
  var duration = 1.5 * 60 * 1000;
  var animationEnd = Date.now() + duration;
  var defaults = { startVelocity: 30, spread: 360, ticks: 60, zIndex: 0 };

  function randomInRange(min, max) {
    return Math.random() * (max - min) + min;
  }

  var interval = setInterval(function () {
    var timeLeft = animationEnd - Date.now();

    if (timeLeft <= 0) {
      return clearInterval(interval);
    }

    var particleCount = 50 * (timeLeft / duration);
    // since particles fall down, start a bit higher than random
    confetti({ ...defaults, particleCount, origin: { x: randomInRange(0.1, 0.3), y: Math.random() - 0.2 } });
    confetti({ ...defaults, particleCount, origin: { x: randomInRange(0.7, 0.9), y: Math.random() - 0.2 } });
  }, 250);
}