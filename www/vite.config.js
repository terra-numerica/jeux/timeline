import { resolve } from "path";
import { defineConfig } from "vite";

export default defineConfig({
  assetsInclude: ["**/*.pdf"],
  base: "/jeux/timeline/",
  build: {
    outDir: "../public",
    rollupOptions: {
      input: {
        main: resolve(__dirname, "index.html"),
        escape: resolve(__dirname, "escape/index.html"),
        room: resolve(__dirname, "escape/room/index.html"),
      },
    },
  },
});
