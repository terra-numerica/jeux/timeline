import "bootstrap/dist/js/bootstrap.esm";
import "reveal.js/dist/reveal.css";
import "reveal.js/dist/theme/simple.css";
import "./style.scss";

import Reveal from "reveal.js";

let deck = new Reveal({
  plugins: [],
  embedded: true,
});
deck.initialize();
