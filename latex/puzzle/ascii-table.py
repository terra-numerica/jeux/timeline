"""Crée l'image du tableau ascii-table.png"""

import os

import numpy as np
import plotly.figure_factory as ff

# on utilise que les 95 caractères imprimables +1 pour le tableau
# 96 = 6*6 ou 4*24
cars = np.arange(32, 128).reshape(4, 24)


def int2bin(i: int) -> str:
    # convertit un entier en binaire sur 7 bits
    return "{0:07b}".format(i)


data = [
    [
        "ascii",
        "caractère",
        "ascii",
        "caractère",
        "ascii",
        "caractère",
        "ascii",
        "caractère",
    ]
]
for i in range(len(cars[0])):
    line = []
    for j in range(len(cars)):
        n = int(cars[j][i])
        line.append(int2bin(n))
        line.append(chr(n))
    data.append(line)

# caractères spéciaux
data[1][1] = "ESPACE"
data[-1][-1] = "DEL"
fig = ff.create_table(data)

fig.update_layout(
    autosize=True,
    width=800,
    height=500,
)

FILE = os.path.join(os.path.realpath(os.path.dirname(__file__)), "ascii-table.png")
fig.write_image(FILE, scale=2)
